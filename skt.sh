#!/bin/bash
source commands/merge.sh
source commands/build.sh
source utils/utils.sh

# Configure our default exit codes.
EXIT_CODE_OK=0
EXIT_CODE_FAIL=1
EXIT_CODE_INFRASTRUCTURE=2

# Set defaults/constants.
STATE="$(pwd)/sktrc"
GITCMD="git -c user.name=skt -c user.email=skt"
WORKDIR="$(pwd)/workdir"
EXIT_CODE=$EXIT_CODE_OK

# read the options
cmd_help() {
    cat <<EOF
------------------------------------------------------------------------------
skt.sh - Tools for merging patches and building kernels
------------------------------------------------------------------------------

Command:

    merge       prepare a git tree and (optionally) merge patches
    build       compile a kernel

Use --help with any command to get help for that specific command.

EOF
}

# Store the command we were asked to run.
CKI_COMMAND=${1:-}

# If we have no command to run, display the help text.
if [ -z ${CKI_COMMAND} ]; then
    cmd_help
    exit 1
fi

case $CKI_COMMAND in
    "--help")
        cmd_help
        ;;
    *)
        shift
        # Does this command exist?
        if typeset -f cmd_${CKI_COMMAND} >/dev/null; then
            cmd_${CKI_COMMAND} $@

            if [[ $EXIT_CODE -eq 0 ]]; then
                log_pass "Success"
            elif [[ $EXIT_CODE -eq 2 ]]; then
                log_fail "Infrastructure failure"
            else
                log_fail "Failure"
            fi
            exit $EXIT_CODE
        else
            echo "Unknown command: ${CKI_COMMAND}"
            cmd_help
        fi
        ;;
esac
