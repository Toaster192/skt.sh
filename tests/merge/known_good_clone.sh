#!/bin/bash

./skt.sh merge --workdir=$WORKDIR --fetch-depth=1 --ref=v4.19.15 \
    --baserepo=https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux.git
