#!/bin/bash

TEST_PATCH="/tmp/acpi-iort-fix-rc_dma_get_range.patch"

curl -s -o $TEST_PATCH \
    https://git.kernel.org/pub/scm/linux/kernel/git/stable/stable-queue.git/plain/queue-4.20/acpi-iort-fix-rc_dma_get_range.patch?id=d6c3d2a64bea72c9d268b809ff10323a9e5da532

./skt.sh merge --workdir=$WORKDIR --fetch-depth=1 --ref=linux-4.20.y \
    --patch=${TEST_PATCH} \
    --baserepo=https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux.git

# Cleanup
rm -f $TEST_PATCH
unset TEST_PATCH
