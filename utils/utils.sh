log_msg() {
    echo "[--] $1"
}

log_pass() {
    echo -e "[\e[32mOK\e[0m] $1"
}

log_fail() {
    echo -e "[\e[31m!!\e[0m] $1"
}

pushd () {
    command pushd "$@" > /dev/null
}

popd () {
    command popd "$@" > /dev/null
}

# Get a value from an INI file.
read_ini() {
    INIFILE=$1
    PARAM=$2
    VALUE=''

    if [ -f "${INIFILE}" ]; then
        if grep -q "^${PARAM} =" $INIFILE; then
            VALUE=$(grep -oP "^${PARAM} = \K(.*)$" $INIFILE)
        fi
    fi

    echo $VALUE
}

# Update an INI file.
update_ini() {
    INIFILE=$1
    PARAM=$2
    VALUE=$3

    # Does the ini file exist?
    if [ -f "$INIFILE" ]; then
        if grep -q "^${PARAM} =" $INIFILE; then
            sed -i "s/^${PARAM} =.*$/${PARAM} = ${VALUE}/" $INIFILE
        else
            echo "${PARAM} = ${VALUE}" >> $INIFILE
        fi
    else
        echo "${PARAM} = ${VALUE}" > $INIFILE
    fi
}

apply_patches() {
    PATCH_TYPE=$1
    PATCHES=$2

    COUNTER=0
    for PATCH in "${PATCHES[@]}"; do
        echo $PATCH_URL
        # If these are patchwork URLs, then we need to add '/mbox/' to the
        # URL before downloading.
        if [ "$PATCH_TYPE" == 'patchwork' ]; then
            PATCH_URL="${PATCH%/}/mbox/"
            PATCH_FILENAME="${TMPDIR}/${COUNTER}.patch"
        fi

        # The local patches don't need to be downloaded.
        if [ "$PATCH_TYPE" == 'localpatch' ]; then
            PATCH_URL=''
            PATCH_FILENAME=$PATCH
        fi

        # Download the patch if needed.
        if [ "${PATCH_URL}" != '' ]; then
            # Download the patch from patchwork
            log_msg "Downloading patch: $PATCH_URL > $PATCH_FILENAME"
            curl --retry 5 --retry-delay 5 -s -o $PATCH_FILENAME \
                $PATCH_URL >> merge.log 2>&1

            # If the patch couldn't be downloaded successfully, note the
            # infrastructure failure.
            if [ $? -ne 0 ]; then
                MERGE_OK=0
                EXIT_CODE=2 # Infrastructure failure
                log_fail "Patch download failed: ${PATCH_URL}"
                break
            fi
        fi

        # Apply the patch.
        log_msg "Applying patch: ${PATCH_FILENAME}"
        $GITCMD am $PATCH_FILENAME >> merge.log 2>&1

        # If the patch couldn't be applied, note the failure and clean up
        # the repository in case the user wants to try again.
        if [ $? -ne 0 ]; then
            MERGE_OK=0
            EXIT_CODE=1
            log_fail "Patch merge failed: ${PATCH_URL}"
            log_msg "Error message from git: "
            tail merge.log
            $GITCMD am --abort
            $GITCMD reset --hard refs/remotes/origin/${REF}
            break
        else
            log_pass "Patch applied successfully."
        fi

        # Finally, add this patch to the statefile.
        PADDED_COUNTER=$(printf '%02d' $COUNTER)
        echo "${PATCH_TYPE}_${PADDED_COUNTER} = ${PATCH}" >> $STATE

        # Increment the counter.
        COUNTER=$(expr $COUNTER + 1)
    done
}

# Generate the Red Hat kernel configs and copy the correct one into place.
make_redhat_configs() {
    RH_CONFIGS_GLOB=$1

    # Build the Red Hat kernel config files.
    log_msg "Building Red Hat kernel config files"
    pushd "${WORKDIR}/source"
        CROSS_COMPILE='' make rh-configs $LOGPIPE && \
            cp -v "${RH_CONFIGS_GLOB}" .config $LOGPIPE
        if [ $? -eq 0 ] && [ -e "${WORKDIR}/source/.config" ]; then
            log_pass "Found Red Hat kernel config file"
        else
            log_fail "Unable to find correct Red Hat kernel config file"
            exit $EXIT_FAILURE
        fi
    popd
}

# Take the user provided config file and run 'olddefconfig' to ensure we will
# be able to build the kernel without user interaction.
make_olddefconfig() {
    CONFIG_FILE=$(realpath $1)

    # Copy the config file to our source directory and run olddefconfig.
    log_msg "Running olddefconfig for $CONFIG_FILE"
    pushd "${WORKDIR}/source"
        cp $CONFIG_FILE . 2>&1 | tee -a $LOGFILE && make olddefconfig 2>&1 | tee -a $LOGFILE
        if [ $? -eq 0 ]; then
            log_pass "Kernel config prepared successfully"
        else
            log_fail "Kernel config preparation failed"
            exit $EXIT_FAILURE
        fi
    popd
}

# Make a kernel config based on an option that the user passed. We expect to
# find this option in the kernel's makefile, such as tinyconfig or
# allyesconfig.
make_makefile_configs() {
    CFGTYPE=$1

    # Try to make the config based on what the user asked for.
    log_msg "Building config with 'make $CFGTYPE'"
    pushd "${WORKDIR}/source"
        make $CFGTYPE $LOGPIPE
        if [ $? -eq 0 ]; then
            log_pass "Config creation completed successfully"
        else
            log_fail "Config creation failed"
            exit $EXIT_FAILURE
        fi
    popd
}
