#!/bin/bash

EXIT_CODE=0
export WORKDIR=$(mktemp -d -t skt-testing-XXXXXXXX)

# Ensure we are using ccache to speed up the builds
export PATH=/usr/lib64/ccache/:${PATH}
ccache -s

# Test merges
for SKT_TEST in tests/merge/*.sh; do
    echo '======================================================================'
    echo "MERGE TEST: $SKT_TEST"
    echo '======================================================================'

    $SKT_TEST
    if [ $? -ne 0 ]; then
        echo "TEST FAILED: $SKT_TEST"
        EXIT_CODE=1
    fi

    rm -rf sktrc ${WORKDIR}/*.log
done

# Test builds
for SKT_TEST in tests/build/*.sh; do
    echo '======================================================================'
    echo "BUILD TEST: $SKT_TEST"
    echo '======================================================================'

    # Do a known good merge.
    tests/merge/known_good_localpatch.sh

    $SKT_TEST
    if [ $? -ne 0 ]; then
        echo "TEST FAILED: $SKT_TEST"
        EXIT_CODE=1
    fi

    rm -rf sktrc ${WORKDIR}/*.log
    pushd ${WORKDIR}/source
        git reset --hard
        git clean -fxd
    popd
done

ccache -s

unset WORKDIR
exit $EXIT_CODE
